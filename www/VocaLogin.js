var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');
/**
 * cordova-plugin-voca-login
 * 로그인 정보 native 입력 플러그인
 */
var VocaLogin = function() {
};
// 사용자 아이디
VocaLogin.userid = null;
// 사용자 이름
VocaLogin.username = null;
// 사용자 학급
VocaLogin.userclass = null;
// 사용자 Cid
VocaLogin.usercid = null;
// 사용자 정보 입력 메소드
VocaLogin.inputUserInfo = function(arg) {
  var self = this;

  var params = Object.assign({}, {
      userid:"",
      username:"",
      userclass:"",
      usercid:""
  }, arg||{});

  exec(function(winParam) {
    self.userid = winParam.userid;
    self.username = winParam.username;
    self.userclass = winParam.userclass;
    self.usercid = winParam.usercid;
  },
  function(error) { console.log(error); },
  "VocaLogin",
  "inputUserInfo",
  [
      params.userid,
      params.username,
      params.userclass,
      params.usercid
  ]
  );
}

module.exports = VocaLogin;

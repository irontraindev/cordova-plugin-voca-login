/**
 * cordova-plugin-voca-login
 * 로그인 정보 native 입력 플러그인
 */

#import <Cordova/CDV.h>

@interface CDVVocaLogin : CDVPlugin

@property NSString* userId;
@property NSString* userName;
@property NSString* userClass;
@property NSString* userCid;

- (void) inputUserInfo:(CDVInvokedUrlCommand *)command;

@end

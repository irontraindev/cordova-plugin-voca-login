# README #

보카트레인 로그인 정보 플러그인입니다.

### 설치 ###

* 코르도바 프로젝트 폴더에서 입력합니다.
* 설치 시
```javascript
cordova plugin add git+ssh://git@bitbucket.org:irontraindev/cordova-plugin-voca-login.git
```
* 삭제 시
```javascript
cordova plugin remove cordova-plugin-voca-login
```

### 사용 ###

* inputUserInfo를 실행하면서 key, value 값을 보냅니다.
```javascript
VocaLogin.inputUserInfo({
	userid: data.uid,
	username: data.uname,
	userclass: data.uclass,
	usercid: data.cid
});
```

### 보카트레인 iOS 기타 호환 사항 ###

* cordova-plugin-keyboard 를 설치 후 적용
```javascript
// index.js 410줄
// index.html 1134줄
// 키보드 플러그인을 써도 ios에서는 show 메소드가 작동은 안하지만 에러는 발생하지 않음
KeyBoard.showKeyboard() --> Keyboard.show();

// index.html 1132 줄
// hide 메소드는 ios에서도 작동 함
KeyBoard.hideKeyboard(); --> Keyboard.hide();
```

* Content-Security-policy 의 default-src 에 gap://ready 추가
```javascript
// index.html 26 줄
<meta http-equiv="Content-Security-Policy" content="default-src * gap://ready; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval'">
```